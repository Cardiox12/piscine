/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_comb2.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbellavi <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/02 18:40:43 by bbellavi          #+#    #+#             */
/*   Updated: 2019/07/03 09:59:29 by bbellavi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

char	get_unit(int nbr)
{
	return ((nbr % 10) + '0');
}

char	get_ten(int nbr)
{
	return ((nbr / 10) + '0');
}

void	format_numbers(int left, int right)
{
	ft_putchar(get_ten(left));
	ft_putchar(get_unit(left));
	ft_putchar(' ');
	ft_putchar(get_ten(right));
	ft_putchar(get_unit(right));
	if (left < 98)
		write(1, ", ", 2);
}

void	ft_print_comb2(void)
{
	int left;
	int right;

	left = 0;
	right = 0;
	while (left <= 98)
	{
		while (right <= 99)
		{
			if (left < right)
				format_numbers(left, right);
			right++;
		}
		right = left + 1;
		left++;
	}
}
