/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sort_int_tab.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbellavi <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/04 11:02:28 by bbellavi          #+#    #+#             */
/*   Updated: 2019/07/04 12:38:37 by bbellavi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	swap(int *a, int *b)
{
	int tmp;

	tmp = *a;
	*a = *b;
	*b = tmp;
}

void	ft_sort_int_tab(int *tab, int size)
{
	const int final_size = size;

	while (size-- > 0)
	{
		if (*tab > *(tab + 1))
		{
			swap(&(*tab), &(*(tab + 1)));
			tab -= (final_size - size);
			size = final_size;
		}
		tab++;
	}
}
