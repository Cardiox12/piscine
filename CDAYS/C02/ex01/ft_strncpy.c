/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbellavi <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/04 13:44:45 by bbellavi          #+#    #+#             */
/*   Updated: 2019/07/05 11:15:39 by bbellavi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

char	ft_strlen(char *str)
{
	const char *final_str = str;

	while (*str)
		str++;
	return (str - final_str);
}

char	*ft_strncpy(char *dest, char *src, unsigned int n)
{
	const int	final_n = n;
	const int	src_len = ft_strlen(src);
	const int	remainder = src_len - n;
	int			offset;

	offset = remainder;
	while (n--)
		*dest++ = *src++;
	if (remainder > 0)
		while (offset--)
			*dest++ = '\0';
	return ((remainder > 0) ? dest - src_len : dest - final_n);
}
