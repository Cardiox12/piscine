/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_is_lowercase.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbellavi <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/04 19:30:16 by bbellavi          #+#    #+#             */
/*   Updated: 2019/07/04 19:35:23 by bbellavi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		is_lowercase(char c)
{
	return ((c >= 'a' && c <= 'z') ? 1 : 0);
}

int		ft_str_is_lowercase(char *str)
{
	while (*str)
	{
		if (!is_lowercase(*str++))
			return (0);
	}
	return (1);
}
