/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcapitalize.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbellavi <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/05 08:23:24 by bbellavi          #+#    #+#             */
/*   Updated: 2019/07/07 09:13:59 by bbellavi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		is_numeric(char c)
{
	return ((c >= '0' && c <= '9') ? 1 : 0);
}

int		is_alpha(char c)
{
	return (((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z')) ? 1 : 0);
}

int		is_uppercase(char c)
{
	return ((c >= 'A' && c <= 'Z') ? 1 : 0);
}

char	*ft_strlowcase(char *str)
{
	const char *final_str = str;

	while (*str)
	{
		if (is_alpha(*str) && is_uppercase(*str))
			*str = *str + 32;
		str++;
	}
	return ((char*)final_str);
}

char	*ft_strcapitalize(char *str)
{
	int index;

	index = 0;
	str = ft_strlowcase(str);
	while (str[index + 1] != '\0')
	{
		if (index == 0 && is_alpha(str[index]))
			str[index] -= 32;
		else if (!is_alpha(str[index]) && is_alpha(str[index + 1])
				&& !is_numeric(str[index]))
			str[index + 1] -= 32;
		index++;
	}
	return (str);
}
