/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlowcase.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbellavi <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/05 08:05:48 by bbellavi          #+#    #+#             */
/*   Updated: 2019/07/05 08:20:17 by bbellavi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		is_uppercase(char c)
{
	return ((c >= 'A' && c <= 'Z') ? 1 : 0);
}

int		is_alpha(char c)
{
	return (((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z')) ? 1 : 0);
}

char	*ft_strlowcase(char *str)
{
	const char *final_str = str;

	while (*str)
	{
		if (is_alpha(*str) && is_uppercase(*str))
			*str = *str + 32;
		str++;
	}
	return ((char*)final_str);
}
