/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putstr_non_printable.c                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbellavi <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/05 13:39:44 by bbellavi          #+#    #+#             */
/*   Updated: 2019/07/07 15:09:46 by bbellavi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

int		is_printable(char c)
{
	return ((c >= 32 && c <= 127) ? 1 : 0);
}

void	write_non_printable(int nb)
{
	const char	*charset = "0123456789abcdef";

	write(1, "\\", 1);
	write(1, &charset[nb / 16], 1);
	write(1, &charset[nb % 16], 1);
}

void	ft_putstr_non_printable(char *str)
{
	while (*str)
	{
		if (!is_printable(*str))
			write_non_printable(*str);
		else
			write(1, str, 1);
		str++;
	}
}
