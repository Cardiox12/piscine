/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strupcase.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbellavi <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/04 22:18:50 by bbellavi          #+#    #+#             */
/*   Updated: 2019/07/05 08:12:35 by bbellavi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		is_alpha(char c)
{
	return (((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z')) ? 1 : 0);
}

int		is_lowercase(char c)
{
	return ((c >= 'a' && c <= 'z') ? 1 : 0);
}

char	*ft_strupcase(char *str)
{
	const char *final_str = str;

	while (*str)
	{
		if (is_lowercase(*str) && is_alpha(*str))
			*str = *str - 32;
		str++;
	}
	return ((char*)final_str);
}
