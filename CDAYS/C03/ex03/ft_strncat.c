/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbellavi <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/07 10:27:10 by bbellavi          #+#    #+#             */
/*   Updated: 2019/07/07 10:47:39 by bbellavi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_strlen(char *str)
{
	const char *final_str = str;

	while (*str)
		str++;
	return (str - final_str);
}

char	*ft_strncpy(char *dest, char *src, unsigned int n)
{
	const char *final_dest = dest;

	while (n--)
		*dest++ = (*src) ? *src++ : '\0';
	*dest = '\0';
	return ((char*)final_dest);
}

char	*ft_strncat(char *dest, char *src, unsigned int nb)
{
	const int dest_len = ft_strlen(dest);

	ft_strncpy(dest + dest_len, src, nb);
	return (dest);
}
