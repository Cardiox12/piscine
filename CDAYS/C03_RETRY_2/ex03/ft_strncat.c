/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbellavi <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/07 10:27:10 by bbellavi          #+#    #+#             */
/*   Updated: 2019/07/10 21:42:31 by bbellavi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#define SIZE 10

int		ft_strlen(char *str)
{
	const char *final_str = str;

	while (*str)
		str++;
	return (str - final_str);
}

char	*ft_strncat(char *dest, char *src, unsigned int nb)
{
	const int dest_len = ft_strlen(dest);
	const int final_n = nb;

	if (nb == 0)
		return (dest);
	dest = dest + dest_len;
	while (nb-- && *src)
		*dest++ = *src++;
	*dest = '\0';
	return (dest - (final_n + dest_len));
}

int		main(int argc, char **argv)
{
	if (argc > 2)
	{
		char buffer_1[SIZE] = "hello";
		char buffer_2[SIZE] = "hello";

		for (int i = 0; i < SIZE ; i++)
			printf("[%i]", buffer_1[i]);

		printf("\n");
		for (int i = 0; i < SIZE ; i++)
			printf("[%i]", buffer_2[i]);

		printf("\n");


		printf("Before :\nOriginal : %s | my : %s\n", buffer_1, buffer_2);

		strncat(buffer_1, argv[1], atoi(argv[2]));
		ft_strncat(buffer_2, argv[1], atoi(argv[2]));
		printf("After :\nOriginal : %s | my : %s\n", buffer_1, buffer_2);
		for (int i = 0; i < SIZE ; i++)
			printf("[%i]", buffer_1[i]);

		printf("\n");
		for (int i = 0; i < SIZE ; i++)
			printf("[%i]", buffer_2[i]);

		printf("\n");

	}
	return (0);
}
