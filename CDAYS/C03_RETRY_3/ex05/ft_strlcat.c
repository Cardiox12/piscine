/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbellavi <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/07 18:39:08 by bbellavi          #+#    #+#             */
/*   Updated: 2019/07/07 23:00:09 by bbellavi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int				ft_strlen(char *str)
{
	const char *final_str = str;

	while (*str)
		str++;
	return (str - final_str);
}

char			*ft_strncpy(char *dest, char *src, unsigned int n)
{
	const char *final_dest = dest;

	while (n--)
		*dest++ = (*src) ? *src++ : '\0';
	return ((char*)final_dest);
}

unsigned int	ft_strlcat(char *dest, char *src, unsigned int size)
{
	const unsigned int src_len = ft_strlen(src);
	const unsigned int dest_len = ft_strlen(dest);

	if (src_len == size || dest_len > size)
		return (src_len + size);
	if (src_len < size - dest_len)
		ft_strncpy(dest + dest_len, src, src_len + 1);
	else
	{
		ft_strncpy(dest + dest_len, src, size - 1);
		dest[dest_len + size - 1] = '\0';
	}
	return (dest_len + src_len);
}
