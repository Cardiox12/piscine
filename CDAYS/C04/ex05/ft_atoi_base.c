/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi_base.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbellavi <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/10 08:36:05 by bbellavi          #+#    #+#             */
/*   Updated: 2019/07/10 09:33:52 by bbellavi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <stdlib.h>

int		ft_strlen(char *str)
{
	const char *final_str = str;

	while (*str)
		str++;
	return (str - final_str);
}

int        base_is_invalid(char *base)
{
	const char *final_base = base;
	int is_space;

	while (*base)
	{
		if (count((char*)final_base, *base) > 1)
			return (1);
		base++;
	}
	if (ft_strlen((char*)final_base) <= 1)
		return (1);
	base = (char*)final_base;
	while (*base)
	{
		is_space = (*base == '\n' || *base == '\t' || *base == '\v' || *base == '\f' || *base == '\r') ? 1 : 0;
		if (*base == '+' || *base == '-' || is_space)
			return (1);
		base++;
	}
	return (0);
}

int		ft_find(char *base,  char c)
{
	int index;

	index = 0;
	while (*base)
	{
		if (*base++ == (unsigned char)c)
			return (index);
		index++;
	}
	return (0);
}

int		ft_pow(int nbr, int power)
{
	const int base = nbr;

	if (nbr == 0)
		return (1);
	while (--power)
		nbr *= base;
	return (nbr);
}

int		ft_atoi_base(char *str, char *base)
{
	char *old_ptr;
	int sign;
	int power;
	int found;
	int result;

	sign = 1;
	result = 0;

	/* Skiping spaces characters */ 
	while (*str && (*str == '\n' || *str == '\t' || *str == '\v' || *str == '\f' || *str == '\r'))
		str++;

	while (*str == '-' || *str == '+')
		sign = (*str++ == '-') ? sign * -1 : 1;

	/* Skiping all letter in bases to calculate the power */
	old_ptr = str;
	while (*str && found(*str, base))
		str++;
	power = str - old_ptr;
	str = old_ptr;

	found = find(*str, base);
	while (found && *str)
		result += found * ft_power(ft_strlen(base), --power);
	return (result * sign);
}

int		main(int argc, char **argv)
{
	if (argc > 2)
		printf("The result of atoi base : %i\n", ft_atoi_base(argv[1], argv[2]));
	return (0);
}
