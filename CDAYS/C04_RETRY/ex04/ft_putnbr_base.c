/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr_base.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbellavi <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/08 12:52:52 by bbellavi          #+#    #+#             */
/*   Updated: 2019/07/09 08:47:40 by bbellavi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

int		is_space(char c)
{
	return ((c == '\n' || c == '\t' || c == '\v' ||
				c == '\f' || c == '\r' || c == ' ') ? 1 : 0);
}

int		ft_strlen(char *str)
{
	const char *final_str = str;

	while (*str)
		str++;
	return (str - final_str);
}

int		count(char *str, int c)
{
	int			counter;

	counter = 0;
	while (*str)
	{
		if (*str++ == (unsigned char)c)
			counter++;
	}
	return (counter);
}

int		base_is_invalid(char *base)
{
	const char *final_base = base;

	while (*base)
	{
		if (count((char*)final_base, *base) > 1)
			return (1);
		base++;
	}
	if (ft_strlen((char*)final_base) <= 1)
		return (1);
	base = (char*)final_base;
	while (*base)
	{
		if (*base == '+' || *base == '-' || is_space(*base))
			return (1);
		base++;
	}
	return (0);
}

void	ft_putnbr_base(int nbr, char *base)
{
	const unsigned int		divisor = ft_strlen(base);
	unsigned int			nb;

	if (base_is_invalid(base))
		return ;
	if (nbr < 0)
	{
		write(1, "-", 1);
		nb = -nbr;
	}
	else
		nb = nbr;
	if (nb >= divisor)
	{
		ft_putnbr_base(nb / divisor, base);
		write(1, &base[nb % divisor], 1);
	}
	else
		write(1, &base[nb % divisor], 1);
}
