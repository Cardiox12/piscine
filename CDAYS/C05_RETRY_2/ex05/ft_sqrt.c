/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sqrt.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbellavi <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/11 09:16:11 by bbellavi          #+#    #+#             */
/*   Updated: 2019/07/18 12:29:06 by bbellavi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_sqrt(int nb)
{
	int sqr;

	sqr = 1;
	while (sqr <= nb / sqr)
	{
		if (sqr == 46341)
			return (0);
		if ((sqr * sqr) == nb)
			return (sqr);
		sqr++;
	}
	return (0);
}
