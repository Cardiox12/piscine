/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_program_name.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbellavi <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/11 14:29:54 by bbellavi          #+#    #+#             */
/*   Updated: 2019/07/12 08:13:59 by bbellavi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

int		main(int argc, char **argv)
{
	const char	*begin = *argv;
	const char	*str = *argv;

	if (argc >= 1)
	{
		while (*str)
			str++;
		write(1, begin, str - begin);
		write(1, "\n", 1);
	}
	return (0);
}
