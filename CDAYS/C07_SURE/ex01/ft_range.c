/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_range.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbellavi <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/12 12:12:17 by bbellavi          #+#    #+#             */
/*   Updated: 2019/07/12 13:11:02 by bbellavi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int		*ft_range(int min, int max)
{
	const int	delta = max - min;
	int			*range;

	if (delta <= 0)
		return (NULL);
	range = (int*)malloc(sizeof(int) * delta);
	if (range == NULL)
		return (NULL);
	while (min < max)
		*range++ = min++;
	return (range - delta);
}
