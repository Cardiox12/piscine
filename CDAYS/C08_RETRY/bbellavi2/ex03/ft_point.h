/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_point.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbellavi <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/16 21:24:39 by bbellavi          #+#    #+#             */
/*   Updated: 2019/07/16 21:30:23 by bbellavi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#	ifndef FT_POINT_H
#	define FT_POINT_H

typedef struct	s_point
{
	int x;
	int y;
}				t_point;

#	endif
