/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_boolean.h                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbellavi <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/16 17:38:26 by bbellavi          #+#    #+#             */
/*   Updated: 2019/07/22 15:57:46 by bbellavi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#	ifndef FT_BOOLEAN_H
#	define FT_BOOLEAN_H

#include <unistd.h>

#	define TRUE 1
#	define FALSE 0
#	define EVEN(nbr) nbr % 2 == 0
#	define EVEN_MSG "I have an even number of arguments.\n"
#	define ODD_MSG "I have an odd number of arguments.\n"
#	define SUCCESS 1

typedef int		t_bool;

#	endif
