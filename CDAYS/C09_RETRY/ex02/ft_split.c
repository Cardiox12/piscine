/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_split.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbellavi <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/21 16:11:34 by bbellavi          #+#    #+#             */
/*   Updated: 2019/07/24 22:48:26 by bbellavi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int		ft_instr(char *str, char c)
{
	int index;

	index = 0;
	while (str[index])
	{
		if (str[index] == c)
			return (1);
		index++;
	}
	return (0);
}

int		ft_count_substrings(char *str, char *charset)
{
	int in_subs;
	int count;

	in_subs = 0;
	count = 0;
	while (*str)
	{
		if (!ft_instr(charset, *str))
		{
			if (!in_subs)
				count++;
			in_subs = 1;
		}
		else
			in_subs = 0;
		str++;
	}
	return (count);
}

int		ft_strlen_tocharset(char *str, char *charset)
{
	int size;

	size = 0;
	while (*str)
	{
		if (ft_instr(charset, *str))
			return (size);
		size++;
		str++;
	}
	return (size);
}

char	*ft_strncpy(char *dest, const char *src, unsigned int bytes)
{
	const char *begin = dest;

	while (*src && bytes)
	{
		*dest++ = *src++;
		bytes--;
	}
	while (bytes--)
		*dest++ = '\0';
	return ((char *)begin);
}

char	**ft_split(char *str, char *charset)
{
	char	**split;
	int		d;
	int		csubs;
	int		len_tocharset;

	d = 0;
	csubs = ft_count_substrings(str, charset);
	if (!(split = malloc((sizeof(char*) * (csubs + 1)))))
		return (NULL);
	while (*str && csubs)
	{
		if (ft_instr(charset, *str))
			str++;
		else
		{
			len_tocharset = ft_strlen_tocharset(str, charset);
			if (!(split[d] = malloc(sizeof(char) * (len_tocharset + 1))))
				return (NULL);
			ft_strncpy(split[d++], str, len_tocharset);
			*(split[d - 1] + len_tocharset) = '\0';
			str += len_tocharset;
		}
	}
	split[d] = 0;
	return (split);
}
