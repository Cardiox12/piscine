/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_map.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbellavi <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/22 14:39:22 by bbellavi          #+#    #+#             */
/*   Updated: 2019/07/25 14:06:22 by bbellavi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int		*ft_map(int *tab, int length, int (*f)(int))
{
	int *modified;
	int *begin;

	modified = malloc(sizeof(int) * length);
	if (modified == NULL)
		return (NULL);
	begin = modified;
	while (length-- > 0)
		*modified++ = f(*tab++);
	return (begin);
}
