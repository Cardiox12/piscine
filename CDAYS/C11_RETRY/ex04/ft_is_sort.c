/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_is_sort.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbellavi <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/22 16:47:19 by bbellavi          #+#    #+#             */
/*   Updated: 2019/07/25 13:59:04 by bbellavi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_is_descending(int *tab, int length, int (*f)(int, int))
{
	int index;

	index = 0;
	while (length-- > 1)
	{
		if (f(tab[index], tab[index + 1]) > 0)
			return (0);
		index++;
	}
	return (1);
}

int		ft_is_increasing(int *tab, int length, int (*f)(int, int))
{
	int index;

	index = 0;
	while (length-- > 1)
	{
		if (f(tab[index], tab[index + 1]) < 0)
			return (0);
		index++;
	}
	return (1);
}

int		ft_is_sort(int *tab, int length, int (*f)(int, int))
{
	if (ft_is_descending(tab, length, f) || ft_is_increasing(tab, length, f))
		return (1);
	return (0);
}
