/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_split.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbellavi <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/21 16:11:34 by bbellavi          #+#    #+#             */
/*   Updated: 2019/07/21 16:11:37 by bbellavi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */


#include <stdio.h>
#include <stdlib.h>

int		ft_separator_is_next(char *str, char *charset)
{
	while (*str == *charset && *charset && *str)
	{
		str++;
		charset++;
	}
	return ((*charset == '\0') ? 1 : 0);
}

int		ft_count_words(char *str, char *charset)
{
	const char	*begin = charset;
	int			counter;

	if (*charset == '\0')
		return (0);
	counter = 0;
	while (*str)
	{
		if (!ft_separator_is_next(str, charset))
		{
			counter++;
			while (*str && ft_separator_is_next(str, charset) != 1)
				str++;

		}
		while (*str && *charset && *charset == *str)
		{
			str++;
			charset++;
		}
		charset = (char*)begin;
	}
	return (counter);
}

int		ft_strlen_sep(char *str, char *sep)
{
	const char *final_str = str;

	while (!ft_separator_is_next(str, sep) && *str)
		str++;
	return (str - final_str);
}

char    **ft_split(char *str, char *charset)
{
    const int   words = ft_count_words(str, charset);
    const char  *final_charset = charset;
    char        **strings;
    int         index;
	int			characters;

    strings = malloc(sizeof(char*) * (words + 1));
    if (strings == NULL)
        return (NULL);
    index = 0;
	characters = 0;
    while (*str && *charset != '\0')
    {
        if (!ft_separator_is_next(str, charset))
        {
            strings[index] = malloc(sizeof(char) * (ft_strlen_sep(str, charset) + 1));
			if (strings[index] == NULL)
				return (NULL);
            while (!ft_separator_is_next(str, charset))
            	strings[index][characters++] = *str++;
			strings[index][characters] = '\0';
            index++;
			characters = 0;
        }
        else
		{
			while (*str == *charset && *str && *charset)
			{
				charset++;
            	str++;
			}
			charset = (char*)final_charset;
		}
    }
    strings[words] = NULL;
    return (strings); 
}

int	main(int argc, char **argv)
{
	if (argc > 2)
	{
		char **array = ft_split(argv[1], argv[2]);
		while (*array != NULL)
			printf("%s\n", *array++);
	}
}