/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbellavi <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/24 22:48:32 by bbellavi          #+#    #+#             */
/*   Updated: 2019/07/24 22:49:10 by bbellavi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
char **ft_split(char*, char*);

int        main(int argc, char **argv)
{
	if (argc > 2)
	{
		char **array = ft_split(argv[1], argv[2]);

		while (*array != NULL)
			printf("%s\n", *array++);
	}
}
