/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   map.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/23 10:19:19 by lperson-          #+#    #+#             */
/*   Updated: 2019/07/23 17:15:56 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MAP_H

# define MAP_H

# define BUFF_SIZE	(32)

typedef struct	s_coordinate
{
	int	x;
	int	y;

}				t_coordinate;

typedef struct	s_map
{
	unsigned int	len_x;
	unsigned int	len_y;
	t_coordinate	*obstacles;
}				t_map;

t_map			ft_parser(const char *pathname, char obstacle);
t_coordinate	*ft_getobstacles(char **map, char obstacle);
t_map			ft_voidmap(void);
int				ft_checkmap(char **map);

#endif
