/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/23 10:28:40 by lperson-          #+#    #+#             */
/*   Updated: 2019/07/23 17:15:35 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "map.h"
#include "io.h"
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include "strings.h"

long			ft_len_file(const char *pathname)
{
	int		filedesc;
	char	buffer[BUFF_SIZE];
	int		bytes;
	long	len_file;

	len_file = 0;
	if ((filedesc = open(pathname, O_RDONLY)) == -1)
		return (-1);
	while ((bytes = read(filedesc, buffer, BUFF_SIZE)) > 0)
		len_file += bytes;
	close(filedesc);
	return (len_file);
}

char			*ft_file_to_str(const char *pathname)
{
	char	*file;
	long	len_file;
	int		filedesc;

	filedesc = open(pathname, O_RDONLY);
	len_file = (filedesc != -1) ? ft_len_file(pathname) : 0;
	file = (char*)malloc(sizeof(char) * (len_file + 1));
	if (!file)
		return (NULL);
	file[len_file] = '\0';
	if (filedesc == -1)
		return (file);
	else
		read(filedesc, file, len_file);
	close(filedesc);
	return (file);
}

t_map			ft_parser(const char *pathname, char obstacle)
{
	char	*file;
	char	**split;
	t_map	map;
	int		i;

	i = 0;
	file = ft_file_to_str(pathname);
	if (!file)
		return (ft_voidmap());
	split = ft_split(file, "\n");
	if (!split)
		return (ft_voidmap());
	if (ft_checkmap(split) == -1)
		return (ft_voidmap());
	while (split[i])
		i++;
	map.len_y = i;
	i = 0;
	while (split[0][i])
		i++;
	map.len_x = i;
	map.obstacles = ft_getobstacles(split, obstacle);
	free(split);
	return (map);
}
