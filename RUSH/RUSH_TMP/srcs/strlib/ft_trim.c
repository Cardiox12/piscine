/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_trim.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbellavi <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/21 18:57:53 by bbellavi          #+#    #+#             */
/*   Updated: 2019/07/21 19:40:20 by aclarman         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <rush.h>

char	*ft_trim(char *str)
{
	char	*trim;
	int		i;
	int		j;
	int		k;

	i = 0;
	j = 0;
	while (str[i] && (str[i] == ' ' || str[i] == '\t'))
		i++;
	while (str[i + j])
		j++;
	while (str[i + j - 1] == ' ' || str[i + j - 1] == '\t')
		j--;
	trim = malloc(sizeof(char) * j + 1);
	if (trim == NULL)
		return (NULL);
	k = 0;
	while (k < j)
	{
		trim[k] = str[i + k];
		k++;
	}
	trim[k] = '\0';
	return (trim);
}
