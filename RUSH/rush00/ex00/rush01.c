/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rush01.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbellavi <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/06 10:35:36 by bbellavi          #+#    #+#             */
/*   Updated: 2019/07/06 13:48:13 by bbellavi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	ft_putchar(char c);

void	ft_putline(int width, char corner_left, char corner_right, char middle)
{
	const int width_max = width;

	width = 0;
	while (width < width_max)
	{
		if (width == 0)
			ft_putchar(corner_left);
		else if (width == width_max - 1)
			ft_putchar(corner_right);
		else
			ft_putchar(middle);
		width++;
	}
}

void	rush(int x, int y)
{
	const int y_max = y;

	y = 0;
	if (x < 0 || y < 0)
		return ;
	while (y < y_max)
	{
		if (y == 0)
			ft_putline(x, '/', '\\', '*');
		else if (y == y_max - 1)
			ft_putline(x, '\\', '/', '*');
		else
			ft_putline(x, '*', '*', ' ');
		ft_putchar('\n');
		y++;
	}
}
