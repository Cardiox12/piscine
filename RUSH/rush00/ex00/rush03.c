/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rush.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lmannier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/07 10:40:33 by lmannier          #+#    #+#             */
/*   Updated: 2019/07/07 22:34:54 by bbellavi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	ft_putchar(char c);

void	ft_putline(int x, char left_corner, char middle, char right_corner)
{
	const int xmax = x;

	x = 0;
	while (x < xmax)
	{
		if (x == 0)
			ft_putchar(left_corner);
		else if (x == xmax - 1)
			ft_putchar(right_corner);
		else
			ft_putchar(middle);
		x++;
	}
}

void	rush(int x, int y)
{
	const int ymax = y;

	y = 0;
	if (x >= 0 && y >= 0)
	{
		while (y < ymax)
		{
			if (y == 0)
				ft_putline(x, 'A', 'B', 'C');
			else if (y == ymax - 1)
				ft_putline(x, 'A', 'B', 'C');
			else
				ft_putline(x, 'B', ' ', 'B');
			ft_putchar('\n');
			y++;
		}
	}
}
