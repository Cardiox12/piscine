/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rush04.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ahenry <ahenry@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/07 12:59:03 by ahenry            #+#    #+#             */
/*   Updated: 2019/07/07 13:03:10 by ahenry           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	ft_putchar(char c);

void	ft_putline(int width, char corner_left, char corner_right, char middle)
{
	const int width_max = width;

	width = 0;
	while (width < width_max)
	{
		if (width == 0)
			ft_putchar(corner_left);
		else if (width == width_max - 1)
			ft_putchar(corner_right);
		else
			ft_putchar(middle);
		width++;
	}
}

void	rush(int x, int y)
{
	const int y_max = y;

	y = 0;
	if (x < 0 || y < 0)
		return ;
	while (y < y_max)
	{
		if (y == 0)
			ft_putline(x, 'A', 'C', 'B');
		else if (y == y_max - 1)
			ft_putline(x, 'C', 'A', 'B');
		else
			ft_putline(x, 'B', 'B', ' ');
		ft_putchar('\n');
		y++;
	}
}
