/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   backtrack.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aaubry <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/13 16:35:28 by aaubry            #+#    #+#             */
/*   Updated: 2019/07/13 20:38:13 by bbellavi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <stdlib.h>
#include "backtrack.h"

int        *ft_str_to_int_vector(char*);

void	print(t_bt *bt)
{
	int i = 0;
	for(i = 0; i < bt->n * bt->n ; i++) {
		printf("%d%s", bt->tab[i], (i % bt->n) == bt->n - 1 ? "\n" : " ");
	}
	getchar();
	system("clear");
}

int		ft_check_doubles(t_bt *bt, int index, int building)
{
	int	row;
	int	col;
	int	i;

	row = index / bt->n;
	col = index % bt->n;
	i = 0;
	while (i < col)
	{
		if (bt->tab[row * bt->n + i] == building)
			return (0);
		i++;
	}
	i = 0;
	while (i < row)
	{
		if (bt->tab[i * bt->n + col] == building)
			return (0);
		i++;
	}
	return (1);
}

int		ft_check_row(t_bt *bt, int index, int building)
{
	int	row;
	int	i;
	int	view;
	int	prev;

	bt->tab[index] = building;
	row = index / bt->n;
	view = 1;
	i = -1;
	prev = bt->tab[row * bt->n];
	while (++i < bt->n)
	{
		if (prev < bt->tab[row * bt->n + i])
		{
			prev = bt->tab[row * bt->n + i];
			view++;
		}
	}
	if (view != bt->views[bt->n * 2 + row])
		return (0);
	view = 1;
	i = -1;
	prev = bt->tab[row * bt->n + bt->n - 1];
	while (++i < bt->n)
	{
		if (prev < bt->tab[row * bt->n + bt->n - i])
		{
			prev = bt->tab[row * bt->n + bt->n - i];
			view++;
		}
	}
	if (view != bt->views[bt->n * 3 + row])
		return (0);
	return (1);
}

int		ft_check_col(t_bt *bt, int index, int building)
{
	int	col;
	int	i;
	int	view;
	int	prev;

	bt->tab[index] = building;
	col = index % bt->n;
	view = 1;
	i = 0;
	prev = bt->tab[col];
	while (++i <= bt->n)
	{
		if (prev < bt->tab[col + i * bt->n])
		{
			prev = bt->tab[col + i * bt->n];
			view++;
		}
	}
	if (view != bt->views[col])
		return (0);
	view = 1;
	i = 0;
	prev = bt->tab[col + bt->n * (bt->n - 1)];
	while (++i <= bt->n)
	{
		if (prev < bt->tab[col + bt->n * (bt->n - i)])
		{
			prev = bt->tab[col + bt->n * (bt->n - i)];
			view++;
		}
	}
	if (view != bt->views[bt->n * 1 + col])
		return (0);
	return (1);
}

int		ft_check(t_bt *bt, int index, int building)
{
	if (ft_check_doubles(bt, index, building) == 0)
		return (0);
	if (index % bt->n == bt->n - 1 && ft_check_row(bt, index, building) == 0)
		return (0);
	if (index / bt->n == bt->n - 1 && ft_check_col(bt, index, building) == 0)
		return (0);
	return (1);
}

int		ft_backtrack(t_bt *bt, int index)
{
	int	building;

	building = 1;
	while (building <= bt->n)
	{
		if (ft_check(bt, index, building))
		{
			bt->tab[index] = building;
			print(bt);
			if (index >= bt->n * bt->n - 1)
				return (1);
			if (ft_backtrack(bt, index + 1) == 1)
				return (1);
		}
		building++;
	}
	bt->tab[index] = 0;
	return (0);
}

int		main(int argc, char **argv)
{
	if (argc > 1)
	{
		int *views = ft_str_to_int_vector(argv[1]);

		if (views)
		{
			int tab[16] = {0};
			t_bt bt = {
				views,
				tab,
				4
			};
			ft_backtrack(&bt, 0);
			print(&bt);
		}
	}
}
