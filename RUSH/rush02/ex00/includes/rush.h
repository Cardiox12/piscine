/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rush.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbellavi <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/21 15:08:46 by bbellavi          #+#    #+#             */
/*   Updated: 2019/07/21 15:26:06 by bbellavi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#	ifndef FT_RUSH_H
#	define FT_RUSH_H

#   define BUFF_SIZE 24
#   define NEWLINE '\n'

#include <dict.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>

void	ft_putchar(char c);
char	*ft_strcat(char *dest, char *src);
void	ft_putstr(char *str);
char    *ft_strdup(char *str);
char	*ft_strncat(char *dest, char *src, int n);
t_dict  *ft_create_dict_list(int size);
t_dict  *ft_create_dict(char *key, char *value);
char    **ft_split(char *str, char *charset);
char    *ft_trim(char *str);
char    *ft_strchr(char *str, int c);
int     ft_strlen(char *str);

#	endif
