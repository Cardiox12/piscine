/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_parse.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bbellavi <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/20 18:35:54 by bbellavi          #+#    #+#             */
/*   Updated: 2019/07/21 15:31:17 by bbellavi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/* RUSH HEADERS */
#include <rush.h>
#include <dict.h>

#define __unused __attribute__((unused))

void	ft_append_to_dynamic_buffer(char **buffer, char *to_append)
{
	char *tmp_ptr;

	if (buffer != NULL)
	{
		if (*buffer == NULL)
			*buffer = ft_strdup(to_append);
		else
		{
			tmp_ptr = *buffer;
			*buffer = ft_strcat(*buffer, to_append);
			free(tmp_ptr);
		}
	}
}

int		ft_count_lines(int fd)
{
	char 	buffer[BUFF_SIZE + 1];
	int		bytes;
	int		counter;

	counter = 0;
	while ((bytes = read(fd, buffer, BUFF_SIZE)))
	{
		buffer[bytes] = '\0';
		if (ft_strchr(buffer, NEWLINE))
			counter++;
	}
	close(fd);
	return (counter);
}

void	ft_resize_buffer(char **buffer)
{
	char *old_ptr;
	char *locate;

	if (buffer != NULL)
	{
		if (*buffer != NULL)
		{
			old_ptr = *buffer;
			locate = ft_strchr(*buffer, NEWLINE);
			if (locate != NULL)
				*buffer = ft_strdup(locate);
			free(old_ptr);
		}
	}
}

t_dict	*ft_get_key_value_from_str(char *str)
{
	t_dict	*dict;
	char	**result;

	result = ft_split(str, DICT_SEPARATOR);
	if (result == NULL)
		return (NULL);
	dict = ft_create_dict(ft_trim(result[0]), ft_trim(result[1]));
	if (dict == NULL)
		return (NULL);
	return (dict);
}

t_dict	*ft_get_dict_from_file(int fd, int size)
{
	t_dict	*dict_list;
	char 	buffer[BUFF_SIZE + 1];
	char 	*dynamic_buffer;
	int		bytes;
	int		index;

	dict_list = ft_create_dict_list(size);
	if (dict_list == NULL)
		return (NULL);
	index = 0;
	while ((bytes = read(fd, buffer, BUFF_SIZE)))
	{
		buffer[bytes] = '\0';
		ft_append_to_dynamic_buffer(&dynamic_buffer, buffer);
		if (ft_strchr(dynamic_buffer, NEWLINE))
		{
			dict_list[index] = *ft_get_key_value_from_str(dynamic_buffer);
			ft_resize_buffer(&dynamic_buffer);
			index++;
		}
	}
	return (dict_list);
}

int		main(int argc, char **argv)
{	
	if (argc > 1)
	{
		int fd;
		int size;

		fd = open(argv[1], O_RDONLY);
		if (fd == -1)
			return (0);
		size = ft_count_lines(fd);

		if (fd == -1)
			return (0);
		fd = open(argv[1], O_RDONLY);
		ft_get_dict_from_file(fd, size);
	}
	return (0);
}